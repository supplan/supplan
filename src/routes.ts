import { RouteRecordRaw } from "vue-router";
import StartPage from "./components/Start/StartPage.vue";
import SyncPage from "./components/Sync/SyncPage.vue";

export const routes: RouteRecordRaw[] = [
    { path: "/", component: StartPage },
    { path: "/sync", component: SyncPage },
];
