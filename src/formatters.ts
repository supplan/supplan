/**
 * Converts bytes to human-readable format.
 *
 * @param bytes - The number of bytes.
 * @param decimals - How many decimal places to include.
 */
export function formatBytes(bytes: number, decimals = 2) {
    // https://gist.github.com/zentala/1e6f72438796d74531803cc3833c039c
    // Thanks zentala
    if (bytes == 0) return "0 bytes";
    const k = 1024,
        sizes = ["bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
        i = Math.floor(Math.log(bytes) / Math.log(k));
    return (bytes / Math.pow(k, i)).toFixed(decimals) + " " + sizes[i];
}

/**
 * Converts a "current" and "max" value into a human readable percentage.
 *
 * @param current - The current value.
 * @param max - The max value, 100%.
 * @param decimals  - How many decimal places to include.
 */
export function formatPercentage(current?: number, max?: number, decimals = 0) {
    if (!current || !max) {
        return (0).toFixed(decimals) + "%";
    }

    return ((current / max) * 100).toFixed(decimals) + "%";
}

/**
 * Simulates `float: right` with text. Will overflow if needed.
 *
 * @param width - Width of the row.
 * @param left - Text to display on the left side of the row.
 * @param right - Text to display on the right side of the row.
 */
export function row(width: number, left?: string, right?: string) {
    left = left || "";
    right = right || "";
    const spaces = Math.max(0, width - left.length - right.length);
    return left + " ".repeat(spaces) + right;
}
