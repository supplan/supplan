// Imports: Vue official
import { createApp } from "vue";
import { createRouter, createWebHashHistory } from "vue-router";

// Imports: Third-party
import { OhVueIcon, addIcons } from "oh-vue-icons";

// Imports: App
import "./style.scss";
import { routes } from "./routes";
import { icons } from "./icons";

import { SyncProvider } from "./providers/Sync/SyncProvider";

import App from "./App.vue";
import BasePanel from "./components/BasePanel.vue";
import BaseShortcut from "./components/BaseShortcut.vue";
import BaseTree from "./components/BaseTree.vue";
import BaseProgressBar from "./components/BaseProgressBar.vue";
import StartBanner from "./components/Start/StartBanner.vue";
import StartLink from "./components/Start/StartLink.vue";
import SyncDatabase from "./components/Sync/SyncDatabase.vue";

// Setup: Vue official
const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

// Setup: Third-party
addIcons(...icons);

// Setup: App
createApp(App)
    .use(router)
    .provide(SyncProvider.Key, new SyncProvider())
    .component("v-icon", OhVueIcon)
    .component("BasePanel", BasePanel)
    .component("BaseShortcut", BaseShortcut)
    .component("BaseTree", BaseTree)
    .component("BaseProgressBar", BaseProgressBar)
    .component("StartBanner", StartBanner)
    .component("StartLink", StartLink)
    .component("SyncDatabase", SyncDatabase)
    .mount("#app");

// Idea: https://www.fda.gov/consumers/health-fraud-scams/health-fraud-product-database
// Idea: detect interactions via API
