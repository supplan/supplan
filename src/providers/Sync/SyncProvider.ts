import { listen, UnlistenFn, Event } from "@tauri-apps/api/event";
import { invoke } from "@tauri-apps/api/tauri";
import { InjectionKey, reactive } from "vue";
import {
    Database,
    DatabaseStatus,
    Download,
    DownloadEvent,
    DownloadEventKind,
    DownloadStatus,
    Sync,
    SyncEvent,
    SyncEventKind,
    SyncStatus,
} from "./types";

/** Provides methods and manages state related to synchronizing databases. */
export class SyncProvider {
    /** SyncProvider injection key. */
    public static Key: InjectionKey<SyncProvider> = Symbol();

    /** Constructs a SyncProvider and calls {@link SyncProvider.listen}. */
    constructor() {
        this.databases = [
            reactive({
                name: "Dietary Supplement Label Database",
                original_source: "US National Institute of Health",
                changed: 0,
                status: DatabaseStatus.Empty,
                download: {
                    id: "nih_ods_dsld",
                    start_command: "download_dsld",
                    status: DownloadStatus.NotStarted,
                    downloaded: 0,
                },
                sync: {
                    id: "nih_ods_dsld",
                    status: SyncStatus.NotStarted,
                    processed: 0,
                    skipped: 0,
                    error_messages: [],
                },
            }),
        ];

        this.listen();
    }

    /** Managed databases. */
    public databases: Database[];

    /** Unsubscribes from the "download" Tauri event. */
    private download_unlisten_fn?: UnlistenFn;

    /** Unsubscribes from the "download" Tauri event. */
    private sync_unlisten_fn?: UnlistenFn;

    /** Subscribes to the "download" Tauri event. */
    public listen() {
        listen("download", (event: Event<DownloadEvent>) => {
            this.handle_download_event(event.payload);
        }).then((unlistenFn) => {
            this.download_unlisten_fn = unlistenFn;
        });

        listen("sync", (event: Event<SyncEvent>) => {
            this.handle_sync_event(event.payload);
        }).then((unlistenFn) => {
            this.sync_unlisten_fn = unlistenFn;
        });
    }

    /**
     * Finds a database, by download id.
     *
     * @param id - The id of the download in the database to find.
     * @throws Throws an error when the database could not be found.
     */
    public get_database_by_download_id(id: string): Database {
        const database = this.databases.find((db) => db.download.id == id);

        if (!database) {
            throw new Error(
                `failed to find database containing download with id '${id}'`
            );
        }

        return database;
    }

    /**
     * Finds a download, by id.
     *
     * @param id - The id of the download to find.
     * @throws Throws an error when the download could not be found.
     */
    public get_download_by_id(id: string): Download {
        const download = this.get_database_by_download_id(id)?.download;

        if (!download) {
            throw new Error(`failed to find download with id '${id}'`);
        }

        return download;
    }

    /**
     * Finds a database, by sync id.
     *
     * @param id - The id of the sync in the database to find.
     * @throws Throws an error when the database could not be found.
     */
    public get_database_by_sync_id(id: string): Database {
        const database = this.databases.find((db) => db.sync.id == id);

        if (!database) {
            throw new Error(
                `failed to find database containing sync with id '${id}'`
            );
        }

        return database;
    }

    /**
     * Finds a sync, by id.
     *
     * @param id - The id of the sync to find.
     * @throws Throws an error when the sync could not be found.
     */
    public get_sync_by_id(id: string): Sync {
        const sync = this.get_database_by_sync_id(id)?.sync;

        if (!sync) {
            throw new Error(`failed to find sync with id '${id}'`);
        }

        return sync;
    }

    /**
     * Starts a download.
     *
     * @param download - The download to start.
     */
    public async start(download: Download) {
        const database = this.get_database_by_download_id(download.id);
        download.status = DownloadStatus.Starting;
        database.status = DatabaseStatus.Downloading;
        invoke(download.start_command, { id: download.id });
    }

    /**
     * Handles a download event and updates state as needed.
     *
     * @param event - The event to handle.
     */
    private handle_download_event(event: DownloadEvent) {
        const database = this.get_database_by_download_id(event.id);
        const download = database.download;

        switch (event.kind) {
            case DownloadEventKind.Started:
                if (!event.content_length) {
                    throw new Error(
                        "received 'Started' event with undefined 'content_length'"
                    );
                }
                download.content_length = event.content_length;
                download.status = DownloadStatus.Active;
                break;
            case DownloadEventKind.Progress:
                if (!event.downloaded) {
                    throw new Error(
                        "received 'Progress' event with undefined 'downloaded'"
                    );
                }
                download.downloaded = event.downloaded;
                break;
            case DownloadEventKind.Complete:
                download.status = DownloadStatus.Complete;
                database.status = DatabaseStatus.Processing;
                break;
            case DownloadEventKind.Error:
                if (!event.error_message) {
                    throw new Error(
                        "received 'Error' event with undefined 'error_message'"
                    );
                }
                download.status = DownloadStatus.Failed;
                download.error_message = event.error_message;
                database.status = DatabaseStatus.Error;
                break;
            default:
                throw new Error(`received unexpected '${event.kind}' event`);
        }
    }

    /**
     * Handles a sync event and updates state as needed.
     *
     * @param event - The event to handle.
     */
    private handle_sync_event(event: SyncEvent) {
        const database = this.get_database_by_sync_id(event.id);
        const sync = database.sync;

        switch (event.kind) {
            case SyncEventKind.Started:
                if (!event.entries) {
                    throw new Error(
                        "received 'Started' event with undefined 'entries'"
                    );
                }
                sync.entries = event.entries;
                sync.status = SyncStatus.Active;
                break;
            case SyncEventKind.Progress:
                if (!event.processed) {
                    throw new Error(
                        "received 'Progress' event with undefined 'processed'"
                    );
                }
                sync.processed = event.processed;
                break;
            case SyncEventKind.Skipped:
                sync.skipped++;
                break;
            case SyncEventKind.Complete:
                sync.status = SyncStatus.Complete;
                database.status = DatabaseStatus.Processing;
                break;
            case SyncEventKind.Error:
                if (!event.error_message) {
                    throw new Error(
                        "received 'Error' event with undefined 'error_message'"
                    );
                }
                sync.error_messages.push(event.error_message);
                sync.skipped++;
                break;
            default:
                throw new Error(`received unexpected '${event.kind}' event`);
        }
    }
}
