/** Model for a managed database. */
export interface Database {
    /** Name of the database. */
    name: string;

    /** A reference to the original source of the database. */
    original_source: string;

    /** Unix timestamp of the last time the database was changed. */
    changed: number;

    /** Status of the database. */
    status: DatabaseStatus;

    /** Represents the remote file download for this database. */
    download: Download;

    /** Represent the sync operation for this database. */
    sync: Sync;
}

/** Various possible statuses for a managed database. */
export enum DatabaseStatus {
    Empty = "Empty",
    Downloading = "Downloading",
    Processing = "Processing",
    Ready = "Ready",
    Error = "Error",
}

/* -------------------------------- DOWNLOAD -------------------------------- */

/** Represents a remote file download. */
export interface Download {
    /** An arbitrary identifier for the download. */
    id: string;

    /** This name of the Tauri command that starts the download. */
    start_command: string;

    /** The status of the tracked download. */
    status: DownloadStatus;

    /** The reported size of the download, in bytes. */
    content_length?: number;

    /** The number of bytes that have been downloaded and written. */
    downloaded: number;

    /** An error message, if an error occured. */
    error_message?: string;
}

/** Various possible statuses for a tracked download. */
export enum DownloadStatus {
    NotStarted = "Not started",
    Starting = "Starting",
    Active = "Active",
    Complete = "Complete",
    Failed = "Failed",
}

/** Kinds of file download events. */
export enum DownloadEventKind {
    Started = "Started",
    Progress = "Progress",
    Complete = "Complete",
    Error = "Error",
}

/**
 * An event which may be received from the backend. Communicates the status of a
 * file download.
 */
export interface DownloadEvent {
    /** An arbitrary identifier for the download that this event pertains to. */
    id: string;

    /** What kind of event this is. */
    kind: DownloadEventKind;

    /** Only present on `Started` events. The size of the download, in bytes. */
    content_length?: number;

    /**
     * Only present on `Progress` events. The number of bytes that have been
     * downloaded and written.
     */
    downloaded?: number;

    /** Only present on `Error` events. An error message. */
    error_message?: string;
}

/* ---------------------------------- SYNC ---------------------------------- */

/** Represents a sync operation. */
export interface Sync {
    /** An arbitrary identifier for the sync operation. */
    id: string;

    /** The status of the tracked sync operation. */
    status: SyncStatus;

    /** The reported number of entries to be synced. */
    entries?: number;

    /** The number of entries that have been synced. */
    processed: number;

    /** The number of entries that have been skipped. */
    skipped: number;

    /** An error message, if an error occured. */
    error_messages: string[];
}

/**
 * An event which may be received from the backend. Communicates the status of a
 * sync operation..
 */
export interface SyncEvent {
    /**
     * An arbitrary identifier for the sync operation that this event pertains
     * to.
     */
    id: string;

    /** What kind of event this is. */
    kind: SyncEventKind;

    /** Only present on `Started` events. The number of entries to sync. */
    entries?: number;

    /**
     * Only present on `Progress` events. The number of entries that have been
     * synced.
     */
    processed?: number;

    /** Only present on `Error` events. An error message. */
    error_message?: string;
}

/** Various possible statuses for a tracked sync operaton. */
export enum SyncStatus {
    NotStarted = "Not started",
    Starting = "Starting",
    Active = "Active",
    Complete = "Complete",
    Failed = "Failed",
}

/** Kinds of sync events. */
export enum SyncEventKind {
    Started = "Started",
    Progress = "Progress",
    Skipped = "Skipped",
    Complete = "Complete",
    Error = "Error",
}
