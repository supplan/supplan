// ? Icons cheat sheet: https://oh-vue-icons.js.org/

import { IconType } from "oh-vue-icons/types/icons";
import {
    SiMoleculer,
    FaPills,
    RiStackFill,
    FaDownload,
    BiGearFill,
    RiSpace,
    FaDatabase,
    FiUs,
} from "oh-vue-icons/icons";

export const icons: IconType[] = [
    SiMoleculer,
    FaPills,
    RiStackFill,
    FaDownload,
    BiGearFill,
    RiSpace,
    FaDatabase,
    FiUs,
];

// TODO: Find a way to DRY this
