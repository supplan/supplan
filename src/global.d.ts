/**
 * Contents of the package.json file.
 */
declare let PACKAGE: {
    /**
     * Package version.
     */
    version: string;
};
