#![allow(clippy::missing_docs_in_private_items)]

use chrono::NaiveDate;
use serde::{self, Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct Contact {
    #[serde(rename(deserialize = "zip"))]
    pub zip: Option<String>,

    #[serde(rename(deserialize = "country"))]
    pub country: Option<String>,

    #[serde(rename(deserialize = "isDistributor"))]
    pub is_distributor: Option<String>,

    #[serde(rename(deserialize = "city"))]
    pub city: Option<String>,

    #[serde(rename(deserialize = "webAddress"))]
    pub web_address: Option<String>,

    #[serde(rename(deserialize = "isManufacturer"))]
    pub is_manufacturer: Option<String>,

    #[serde(rename(deserialize = "contactType"))]
    pub contact_type: Option<String>,

    #[serde(rename(deserialize = "isPackager"))]
    pub is_packager: Option<String>,

    #[serde(rename(deserialize = "isReseller"))]
    pub is_reseller: Option<String>,

    #[serde(rename(deserialize = "phoneNumber"))]
    pub phone_number: Option<String>,

    #[serde(rename(deserialize = "streetAddress"))]
    pub street_address: Option<String>,

    #[serde(rename(deserialize = "name"))]
    pub name: Option<String>,

    #[serde(rename(deserialize = "state"))]
    pub state: Option<String>,

    #[serde(rename(deserialize = "isOther"))]
    pub is_other: Option<String>,

    #[serde(rename(deserialize = "email"))]
    pub email: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct DietarySupplementsFact {
    #[serde(rename(deserialize = "driFootNotes"))]
    pub dri_foot_notes: Option<String>,

    #[serde(rename(deserialize = "otherTargetGroups"))]
    pub other_target_groups: Option<String>,

    #[serde(
        rename(deserialize = "servingsPerContainer"),
        deserialize_with = "crate::serde_fn::de_opt_str_to_i32"
    )]
    pub servings_per_container: Option<i32>,

    #[serde(rename(deserialize = "labelFootNotes"))]
    pub label_foot_notes: Option<String>,

    #[serde(rename(deserialize = "dailyValueTargetGroups"))]
    pub daily_value_target_groups: Option<String>,

    #[serde(rename(deserialize = "targetGroupId"))]
    pub target_group_id: Option<String>,

    #[serde(rename(deserialize = "ingredients"))]
    pub ingredients: Option<Vec<Ingredient>>,

    #[serde(rename(deserialize = "servingSizeUnitName"))]
    pub serving_size_unit_name: Option<String>,

    #[serde(
        rename(deserialize = "servingSizeQuantity"),
        deserialize_with = "crate::serde_fn::de_opt_str_to_i32"
    )]
    pub serving_size_quantity: Option<i32>,

    #[serde(rename(deserialize = "otheringredients"))]
    pub other_ingredients: Option<KeyValuePair>,

    #[serde(rename(deserialize = "targetGroupName"))]
    pub target_group_name: Option<String>,

    #[serde(rename(deserialize = "usageSuggestion"))]
    pub usage_suggestion: Option<KeyValuePair>,
}

#[derive(Serialize, Deserialize)]
pub struct Event {
    #[serde(
        rename(deserialize = "date"),
        deserialize_with = "crate::serde_fn::de_opt_str_to_date_bdy",
        serialize_with = "crate::serde_fn::ser_opt_date_to_str"
    )]
    pub date: Option<NaiveDate>,

    #[serde(rename(deserialize = "name"))]
    pub name: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct Ingredient {
    #[serde(rename(deserialize = "childInfo"))]
    pub child_info: Option<Vec<IngredientChild>>,

    #[serde(rename(deserialize = "data"))]
    pub data: Option<IngredientData>,

    #[serde(
        rename(deserialize = "dvPercent"),
        deserialize_with = "crate::serde_fn::de_opt_str_to_i32"
    )]
    pub dv_percent: Option<i32>,

    #[serde(rename(deserialize = "name"))]
    pub name: Option<String>,

    #[serde(rename(deserialize = "altName"))]
    pub alt_name: Option<String>,

    #[serde(rename(deserialize = "dvFactor"))]
    pub dv_factor: Option<f64>,
}

#[derive(Serialize, Deserialize)]
pub struct IngredientChild {
    #[serde(rename(deserialize = "prefix"))]
    pub prefix: Option<String>,

    #[serde(rename(deserialize = "percentage"))]
    pub percentage: Option<String>,

    #[serde(rename(deserialize = "name"))]
    pub name: Option<String>,

    #[serde(rename(deserialize = "altName"))]
    pub alt_name: Option<String>,

    #[serde(rename(deserialize = "botanicalInfo"))]
    pub botanical_info: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct IngredientData {
    #[serde(rename(deserialize = "blendAncestry"))]
    pub blend_ancestry: Option<String>,

    #[serde(rename(deserialize = "relationalOperatorOperatorSymbol"))]
    pub relational_operator_symbol: Option<String>,

    #[serde(rename(deserialize = "ingredientEntrySubspecies"))]
    pub ingredient_entry_subspecies: Option<String>,

    #[serde(
        rename(deserialize = "sfbQuantityQuantity"),
        deserialize_with = "crate::serde_fn::de_opt_str_to_i32"
    )]
    pub sfb_quantity: Option<i32>,

    #[serde(rename(deserialize = "unitName"))]
    pub unit_name: Option<String>,

    #[serde(
        rename(deserialize = "sfbDvPercent"),
        deserialize_with = "crate::serde_fn::de_opt_str_to_f64"
    )]
    pub sfb_dv_percent: Option<f64>,

    #[serde(rename(deserialize = "ingredientEntryPlantPart"))]
    pub ingredient_entry_plant_part: Option<String>,

    #[serde(rename(deserialize = "ingredientEntryNotes"))]
    pub ingredient_entry_notes: Option<String>,

    #[serde(rename(deserialize = "ingredientEntryGenus"))]
    pub ingredient_entry_genus: Option<String>,

    #[serde(rename(deserialize = "ingredientEntryCategory"))]
    pub ingredient_entry_category: Option<String>,

    #[serde(rename(deserialize = "ingredientEntrySpecies"))]
    pub ingredient_entry_species: Option<String>,

    #[serde(rename(deserialize = "ingredientEntryIpSymbolId"))]
    pub ingredient_entry_ip_symbol_id: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct KeyValuePair {
    #[serde(rename(deserialize = "text"))]
    pub value: Option<String>,

    #[serde(rename(deserialize = "type"))]
    pub key: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct Label {
    #[serde(
        rename(deserialize = "dsldId"),
        deserialize_with = "crate::serde_fn::de_str_to_i32"
    )]
    pub dsld_id: i32,

    #[serde(rename(deserialize = "langualProductType"))]
    pub langual_product_type: Option<String>,

    #[serde(
        rename(deserialize = "entryDate"),
        deserialize_with = "crate::serde_fn::de_opt_str_to_date_ymd",
        serialize_with = "crate::serde_fn::ser_opt_date_to_str"
    )]
    entry_date: Option<NaiveDate>,

    #[serde(rename(deserialize = "labelVersion"))]
    pub label_version: Option<String>,

    #[serde(rename(deserialize = "statementGroups"))]
    pub statement_groups: Option<Vec<StatementGroup>>,

    #[serde(rename(deserialize = "productName"))]
    pub product_name: Option<String>,

    #[serde(rename(deserialize = "langualClaimsOrUses"))]
    pub langual_claims_or_uses: Option<String>,

    #[serde(rename(deserialize = "dietarySupplementsFacts"))]
    pub dietary_supplements_facts: Option<Vec<DietarySupplementsFact>>,

    #[serde(rename(deserialize = "langualCodes"))]
    pub langual_codes: Option<LangualCodeGroup>,

    #[serde(rename(deserialize = "langualTargetGroup"))]
    pub langual_target_group: Option<String>,

    #[serde(rename(deserialize = "statementOfIdentity"))]
    pub statement_of_identity: Option<String>,

    #[serde(rename(deserialize = "netContentQuantities"))]
    pub net_content_quantities: Option<String>,

    #[serde(
        rename(deserialize = "offMarket"),
        deserialize_with = "crate::serde_fn::de_opt_str_to_bool"
    )]
    pub off_market: Option<bool>,

    #[serde(rename(deserialize = "langualSupplementForm"))]
    pub langual_supplement_form: Option<String>,

    #[serde(rename(deserialize = "nhanesId"))]
    pub nhanes_id: Option<String>,

    #[serde(rename(deserialize = "brand"))]
    pub brand: Option<String>,

    #[serde(rename(deserialize = "outerPackaging"))]
    pub outer_packaging: Option<String>,

    #[serde(rename(deserialize = "servingSize"))]
    pub serving_size: Option<Vec<ServingSize>>,

    #[serde(rename(deserialize = "events"))]
    pub events: Option<Vec<Event>>,

    #[serde(rename(deserialize = "contacts"))]
    pub contacts: Option<Vec<Contact>>,

    #[serde(rename(deserialize = "suggestedUse"))]
    pub suggested_use: Option<String>,

    #[serde(rename(deserialize = "ipSymbol"))]
    pub ip_symbol: Option<String>,

    #[serde(rename(deserialize = "sku"))]
    pub sku: Option<String>,
}

// #[derive(Serialize, Deserialize)]
// pub struct LangualCode {
//     facet_term_code: String,

//     term: Option<String>,
// }

#[derive(Serialize, Deserialize)]
pub struct LangualCodeGroup {
    #[serde(rename(deserialize = "targetGroup"))]
    pub target_group: Option<Vec<String>>,

    #[serde(rename(deserialize = "claimsOrUses"))]
    pub claims_or_uses: Option<Vec<String>>,

    #[serde(rename(deserialize = "supplementForm"))]
    pub supplement_form: Option<Vec<String>>,

    #[serde(rename(deserialize = "productType"))]
    pub product_type: Option<Vec<String>>,
}

#[derive(Serialize, Deserialize)]
pub struct LangualDescriptor {
    #[serde(rename(deserialize = "$unflatten=FTC"))]
    pub facet_term_code: String,

    #[serde(rename(deserialize = "$unflatten=TERM"))]
    pub term: String,

    #[serde(rename(deserialize = "$unflatten=BT"))]
    pub broader_term: Option<String>,

    #[serde(rename(deserialize = "$unflatten=SN"))]
    pub scope_note: Option<String>,

    #[serde(rename(deserialize = "$unflatten=AI"))]
    pub additional_info: Option<String>,

    #[serde(rename(deserialize = "$unflatten=SYNONYMS"))]
    pub synonyms: Option<Vec<String>>,

    #[serde(rename(deserialize = "$unflatten=RELATEDTERMS"))]
    pub related_terms: Option<Vec<String>>,

    #[serde(
        rename(deserialize = "$unflatten=CLASSIFICATION"),
        deserialize_with = "crate::serde_fn::de_str_to_bool"
    )]
    pub classification: bool,

    #[serde(rename(deserialize = "$unflatten=ACTIVE"))]
    pub active: bool,

    #[serde(
        rename(deserialize = "$unflatten=DATEUPDATED"),
        deserialize_with = "crate::serde_fn::de_str_to_date_ymd",
        serialize_with = "crate::serde_fn::ser_date_to_str"
    )]
    pub date_updated: NaiveDate,

    #[serde(
        rename(deserialize = "$unflatten=DATECREATED"),
        deserialize_with = "crate::serde_fn::de_str_to_date_ymd",
        serialize_with = "crate::serde_fn::ser_date_to_str"
    )]
    pub date_created: NaiveDate,

    #[serde(rename(deserialize = "$unflatten=UPDATECOMMENT"))]
    pub update_comment: Option<String>,

    #[serde(rename(deserialize = "$unflatten=SINGLE"))]
    pub single: bool,
}

#[derive(Serialize, Deserialize)]
pub struct ServingSize {
    #[serde(rename(deserialize = "size"))]
    pub size: Option<String>,

    #[serde(rename(deserialize = "type"))]
    pub serving_size_type: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct StatementGroup {
    #[serde(rename(deserialize = "groupName"))]
    pub group_name: Option<String>,

    #[serde(rename(deserialize = "statements"))]
    pub statements: Option<Vec<String>>,
}
