use serde::Serialize;

/// Kinds of updates about the status of an archive sync operation.
#[derive(Serialize)]
#[serde(tag = "kind")]
pub enum EventData<'a> {
    /// Signals that a processing has started.
    Started {
        /// The number of file entries in the archive.
        entries: u32,
    },

    /// Signals the progress of a sync.
    Progress {
        /// The number of bytes that have been downloaded.
        processed: u32,
    },

    /// Signals that an entry was skipped.
    Skipped,

    /// Signals that a sync has completed successfully.
    Complete,

    /// Signals that syncing an entry failed.
    Error {
        /// An error message to aid in debugging.
        error_message: &'a String,
    },
}

/// Wraps [`EventData`] to include the sync id.
#[derive(Serialize)]
pub struct Event<'a> {
    /// Signals which sync operation this event pertains to.
    pub id: &'a str,

    /// The event's payload.
    #[serde(flatten)]
    pub data: EventData<'a>,
}
