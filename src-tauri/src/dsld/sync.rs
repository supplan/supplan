use core::arch;
use std::{
    borrow::BorrowMut,
    fs::File,
    io::Read,
    path::PathBuf,
    sync::{
        atomic::{AtomicU32, Ordering},
        Arc,
    },
    time::Instant,
};

use antidote::Mutex;
use dashmap::mapref::entry;
use rayon::prelude::*;
use tokio::task;
use zip::ZipArchive;

use crate::dsld::{Event, EventData};

use super::{archive_manager::ThreadSafeArchiveManager, models::Label};

/// Errors that may occur when extracting and processing the DSLD JSON archive.
#[derive(thiserror::Error, Debug)]
pub enum SyncError {
    /// Occurs when the [`ThreadSafeArchiveManager`] encounters an error.
    #[error("archive manager error ({0})")]
    ArchiveManager(#[source] super::archive_manager::ArchiveManagerError),

    /// Occurs when the [`ThreadSafeArchiveManager`] encounters an error.
    #[error("archive manager failed to provide the archive")]
    Archive,

    /// Occurs when reading the JSON archive fails.
    #[error("failed to read archive file '{1}' ({0})")]
    ArchiveRead(#[source] zip::result::ZipError, PathBuf),

    /// Occurs when the archive has zero entries.
    #[error("archive '{0}' has zero entries")]
    Empty(PathBuf),

    /// Occurs when reading an entry's info in the JSON archive fails.
    #[error("failed to read info for archive entry [{1}] ({0})")]
    Entry(#[source] zip::result::ZipError, usize),

    /// Occurs when reading an archive entry to a buffer fails.
    #[error("failed to read archive entry '{1}' to buffer ({0})")]
    Buffer(#[source] std::io::Error, String),

    /// Occurs when deserializing an archive entry fails.
    #[error("failed to deserialize archive entry '{1}' ({0})")]
    Deserialize(#[source] serde_json::Error, String),
}

/// Processes the DSLD JSON archive decompressing and deserializing entries into
/// memory, in parallel.
///
/// # Arguments
/// * `source_file` - The path of the DSLD JSON archive to process.
///
/// # Errors
///
/// This function will return an error if any error occurs when reading the
/// archive.
pub fn process_archive(
    id: &str,
    archive_man: tauri::State<'_, ThreadSafeArchiveManager>,
    emit_fn: impl Fn(Event) + std::marker::Sync,
) -> Result<(), self::SyncError> {
    let emit = |data: EventData| emit_fn(Event { id, data });

    let source_file = archive_man
        .lock()
        .file_path()
        .map_err(SyncError::ArchiveManager)?;

    log::info!("processing archive '{}'", source_file.as_path().display());

    let len = archive_man
        .lock()
        .len()
        .map_err(SyncError::ArchiveManager)?;

    emit(EventData::Started {
        entries: len as u32,
    });

    let processed = AtomicU32::new(0);

    if len == 0 {
        return Err(SyncError::Empty(source_file));
    }

    let now = Instant::now();
    let archive = archive_man.lock().archive();

    (0..len)
        .into_par_iter()
        .filter_map(|i| match &archive {
            Ok(archive) => match archive.lock().by_index(i) {
                Ok(mut entry) if entry.is_file() => {
                    let mut buffer = Vec::new();

                    match entry.read_to_end(&mut buffer) {
                        Ok(_) => Some(buffer),
                        Err(err) => {
                            emit(EventData::Error {
                                error_message: &err.to_string(),
                            });
                            None
                        }
                    }
                }
                Ok(_) => {
                    emit(EventData::Skipped);
                    None
                }
                Err(err) => {
                    emit(EventData::Error {
                        error_message: &err.to_string(),
                    });
                    None
                }
            },
            Err(_) => None,
        })
        .for_each(|buffer| {
            let model = serde_json::from_slice::<Label>(buffer.as_slice());
            let p = processed.fetch_add(1, Ordering::Relaxed) + 1;

            // todo: index model with tantivy
            // https://github.com/quickwit-oss/tantivy

            // if p % (len as u32 / 10) == 0 {
            emit(EventData::Progress { processed: p })
            // }
        });

    emit(EventData::Complete);

    log::info!(
        "finished processing {} entries, took {} ms",
        processed.load(Ordering::Relaxed),
        now.elapsed().as_millis()
    );

    Ok(())
}
