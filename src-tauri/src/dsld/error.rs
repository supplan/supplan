use super::archive_manager;
use super::scraper;
use super::sync;
use crate::downloader;

/// Errors that may occur when managing DSLD database.
#[derive(thiserror::Error, Debug)]
pub enum DsldError {
    /// Occurs when locating the download directory fails.
    #[error("locating downloads directory failed ({0})")]
    ArchiveManager(#[from] archive_manager::ArchiveManagerError),

    /// Occurs when scraping the DSLD JSON archive download URL fails.
    #[error("scraping failed ({0})")]
    Scrape(#[from] scraper::ScrapeError),

    /// Occurs when downloading the DSLD JSON archive fails.
    #[error("download failed ({0})")]
    Download(#[from] downloader::DownloaderError),

    /// Occurs when extracting and processing the DSLD JSON archive fails.
    #[error("processing failed ({0})")]
    Sync(#[from] sync::SyncError),
}
