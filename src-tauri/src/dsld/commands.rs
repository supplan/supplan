use dashmap::DashMap;

use crate::{
    downloader::{self, State},
    dsld::error::DsldError,
    events::{self, OutEventPayload},
};

use super::{
    archive_manager::ThreadSafeArchiveManager, scraper::download_url, sync,
};

/// Downloads the [NIH ODS DSLD][dsld] JSON archive.
///
/// [dsld]: https://dsld.od.nih.gov/
#[tauri::command]
pub async fn download_dsld(
    app_handle: tauri::AppHandle,
    states: tauri::State<'_, DashMap<String, State>>,
    archive_man: tauri::State<'_, ThreadSafeArchiveManager>,
    id: String,
) -> Result<(), String> {
    let result: Result<(), DsldError> = async {
        let source_url = download_url().await.map_err(DsldError::Scrape)?;
        let target_file = archive_man
            .lock()
            .file_path()
            .map_err(DsldError::ArchiveManager)?;

        log::info!(
            "downloading DSLD from '{source_url}' to '{}'",
            target_file.display()
        );

        downloader::download_file(
            &states,
            id.as_str(),
            source_url,
            target_file.clone(),
            |event| {
                events::emit(&app_handle, &OutEventPayload::Download(&event));
            },
        )
        .await
        .map_err(DsldError::Download)?;

        archive_man
            .lock()
            .load_archive()
            .map_err(DsldError::ArchiveManager)?;

        sync::process_archive(id.as_str(), archive_man, |event| {
            events::emit(&app_handle, &OutEventPayload::Sync(&event));
        })
        .map_err(DsldError::Sync)?;

        Ok(())
    }
    .await;

    if let Err(err) = result {
        let msg = format!("sync failed (id: {id}) ({err})");
        log::error!("{msg}");
        return Err(msg);
    }

    Ok(())
}
