/// Errors that may occur when handling DSLD-related operations.
mod error;
pub use error::*;

/// Events relevant to syncing the archive.
mod events;
pub use events::*;

/// Models from the DSLD API specification, version 8.
pub mod models;

/// Provides Tauri commands.
pub mod commands;

/// Handles extracting, processing, and syncing the downloaded JSON archive into
/// a local database.
pub mod sync;

/// Handles loading and managing the DSLD archive.
pub mod archive_manager;

/// Provides a function for scraping the DSLD download URL.
pub mod scraper;
