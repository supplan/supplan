use reqwest::{Client, Url};
use scraper::{Html, Selector};
use tauri::regex::Regex;

/// The DSLD base URL.
const URL_BASE: &str = "https://dsld.od.nih.gov/";

/// The URL of the API Guide document.
const URL_API_GUIDE: &str = "api-guide";

/// The selector used to file script elements.
const SCRIPT_SELECTOR: &str = "script[src][defer]";

/// The regex to use to extract the JSON archive download URL.
const REGEX_DOWNLOAD_URL: &str = r#"fullJSONPath:"([^"]+)"#;

/// Errors that may occur when scraping the download URL for the DLSD.
#[derive(thiserror::Error, Debug)]
pub enum ScrapeError {
    /// Occurs when a GET request fails.
    #[error("request to '{1}' failed ({0})")]
    Request(#[source] reqwest::Error, String),

    /// Occurs when a getting the body from a response fails.
    #[error("failed to get response body from url '{1}' ({0})")]
    Body(#[source] reqwest::Error, String),

    /// Occurs when no script elements were found with the match criteria.
    #[error("failed to parse selector '{0}'")]
    Selector(String),

    /// Occurs when no script elements were found with the match criteria.
    #[error("failed to find main script")]
    Script,

    /// Occurs when compiling the download URL extractor regex fails.
    #[error("compiling regex '{1}' failed ({0})")]
    RegexCompile(#[source] tauri::regex::Error, String),

    /// Occurs when extracting the download URL via regex fails.
    #[error("download url regex capture failed")]
    RegexExtract,

    /// Occurs when joining URLs fails.
    #[error("failed to join urls '{1}' + '{2}' ({0})")]
    UrlJoin(#[source] url::ParseError, String, String),

    /// Occurs when parsing a URL fails.
    #[error("failed to parse url '{1}' ({0})")]
    UrlParse(#[source] url::ParseError, String),
}

/// Scrapes the DSLD API Guide page to find the JSON archive download URL.
///
/// # Errors
///
/// This function will return an error if requesting or scraping fails.
pub async fn download_url() -> Result<Url, ScrapeError> {
    let client = Client::new();
    let base_url = Url::parse(URL_BASE)
        .map_err(|e| ScrapeError::UrlParse(e, URL_BASE.to_string()))?;
    let document_url = base_url.join("api-guide").map_err(|e| {
        ScrapeError::UrlJoin(e, URL_BASE.to_string(), URL_API_GUIDE.to_string())
    })?;

    let document_body = client
        .get(document_url.clone())
        .send()
        .await
        .map_err(|e| ScrapeError::Request(e, document_url.to_string()))?
        .text()
        .await
        .map_err(|e| ScrapeError::Body(e, document_url.to_string()))?;

    let script_url = {
        let document = Html::parse_document(&document_body);
        let script_selector = Selector::parse(SCRIPT_SELECTOR)
            .map_err(|_| ScrapeError::Selector(SCRIPT_SELECTOR.to_string()))?;
        let script_name = document
            .select(&script_selector)
            .filter_map(|script| script.value().attr("src"))
            .find(|src| src.starts_with("main-"))
            .ok_or(ScrapeError::Script)?;

        let script_url = base_url.join(script_name).map_err(|e| {
            ScrapeError::UrlJoin(
                e,
                base_url.to_string(),
                script_name.to_string(),
            )
        })?;

        Ok::<Url, ScrapeError>(script_url)
    }?;

    let script_body = client
        .get(script_url.clone())
        .send()
        .await
        .map_err(|e| ScrapeError::Request(e, script_url.to_string()))?
        .text()
        .await
        .map_err(|e| ScrapeError::Body(e, script_url.to_string()))?;

    let regex = Regex::new(REGEX_DOWNLOAD_URL).map_err(|e| {
        ScrapeError::RegexCompile(e, REGEX_DOWNLOAD_URL.to_string())
    })?;

    let download_url = regex
        .captures(script_body.as_str())
        .ok_or(ScrapeError::RegexExtract)?
        .get(1)
        .ok_or(ScrapeError::RegexExtract)?
        .as_str();

    let download_url_validated = Url::parse(download_url)
        .map_err(|e| ScrapeError::UrlParse(e, download_url.to_string()))?;

    Ok(download_url_validated)
}
