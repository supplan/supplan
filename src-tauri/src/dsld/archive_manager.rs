use std::{
    fs::{create_dir_all, File},
    path::PathBuf,
    sync::Arc,
    time::Instant,
};

use antidote::Mutex;
use tauri;
use zip::ZipArchive;

/// Errors that may occur when managing DSLD database.
#[derive(thiserror::Error, Debug)]
pub enum ArchiveManagerError {
    /// Occurs when resolving the path to the data directory fails.
    #[error("resolving path to data directory failed")]
    ResolveDir,

    /// Occurs when creating the archive directory in the data directory fails.
    #[error("creating archive directory in data directory failed")]
    CreateDir(#[source] std::io::Error),

    /// Occurs when checking if the archive file exists fails.
    #[error("creating archive directory in data directory failed")]
    CheckExists(#[source] std::io::Error),

    /// Occurs when opening the JSON archive fails.
    #[error("failed to open archive file '{1}' ({0})")]
    Open(#[source] std::io::Error, PathBuf),

    /// Occurs when reading the JSON archive fails.
    #[error("failed to read archive file '{1}' ({0})")]
    Archive(#[source] zip::result::ZipError, PathBuf),

    #[error("todo: write error message lol")]
    ArchiveNotLoaded,
}

/// An ArchiveManager in a [`Mutex`] in an [`Arc`].
pub type ThreadSafeArchiveManager = Arc<Mutex<ArchiveManager>>;
pub type ThreadSafeArchive = Arc<Mutex<ZipArchive<File>>>;

/// Handles loading the DSLD archive, and manages access to it.
pub struct ArchiveManager {
    /// The managed archive.
    _archive: Option<ThreadSafeArchive>,

    /// The path to the archive file.
    _file: Option<PathBuf>,
}

impl ArchiveManager {
    /// Instantiates a new ArchiveManager.
    pub fn new() -> Self {
        Self {
            _archive: None,
            _file: None,
        }
    }

    /// Computes the file path to the DSLD archive. This does not guarantee that
    /// the file exists, but it will create directories as needed.
    ///
    /// # Errors
    ///
    /// This function returns an error if the path to the data directory cannot
    /// be resolved, or if creating the archive directory in the data directory
    /// fails.
    pub fn file_path(&mut self) -> Result<PathBuf, ArchiveManagerError> {
        if self._file.is_none() {
            let dir = tauri::api::path::data_dir()
                .ok_or(ArchiveManagerError::ResolveDir)?
                .join("archive");
            self._file = Some(dir.join("nih_ods_dsld").with_extension("zip"));
            create_dir_all(dir).map_err(ArchiveManagerError::CreateDir)?;
        }

        Ok(self._file.to_owned().unwrap())
    }

    pub fn exists(&mut self) -> Result<bool, ArchiveManagerError> {
        let result = self
            .file_path()?
            .try_exists()
            .map_err(ArchiveManagerError::CheckExists)?;

        Ok(result)
    }

    pub fn load_archive(&mut self) -> Result<(), ArchiveManagerError> {
        if self._archive.is_none() {
            let file_path = self.file_path()?;

            let file = File::open(&file_path).map_err(|e| {
                ArchiveManagerError::Open(e, file_path.to_owned())
            })?;

            let now = Instant::now();

            self._archive =
                Some(Arc::new(Mutex::new(ZipArchive::new(file).map_err(
                    |e| ArchiveManagerError::Archive(e, file_path.to_owned()),
                )?)));

            log::info!(
                "loaded archive '{}' in {} ms",
                file_path.as_path().display(),
                now.elapsed().as_millis()
            );
        }

        Ok(())
    }

    pub fn archive(&self) -> Result<ThreadSafeArchive, ArchiveManagerError> {
        match self._archive.clone() {
            Some(archive) => Ok(archive),
            None => Err(ArchiveManagerError::ArchiveNotLoaded),
        }
    }

    pub fn len(&self) -> Result<usize, ArchiveManagerError> {
        Ok(self.archive()?.lock().len())
    }

    // pub fn by_index(&self, i: usize) -> Result<ZipFile, ArchiveManagerError> {
    //     Ok(self
    //         .archive()?
    //         .lock()
    //         .by_index(i)
    //         .map_err(|e| ArchiveManagerError::Entry(e, i))?)
    // }

    // /// todo: document this
    // pub fn get() {
    //     let x = Arc::new(Mutex::new )
    // }
}
