use serde::Serialize;
use tauri::{AppHandle, Manager};

use crate::{downloader, dsld};

/// Kinds of event payloads to be emitted to the client.
#[derive(Clone, Serialize)]
#[serde(untagged)]
pub enum OutEventPayload<'a> {
    /// Signals the status of a file download.
    Download(&'a downloader::Event<'a>),
    Sync(&'a dsld::Event<'a>),
}

/// Returns the proper event identifier to use for this payload.
pub fn event_name<'a>(payload: &'a OutEventPayload) -> &'a str {
    match payload {
        OutEventPayload::Download(_) => "download",
        OutEventPayload::Sync(_) => "sync",
    }
}

/// Wraps Tauri's `emit_all` to make sure we always send event payloads with the
/// correct event name.
pub fn emit(app_handle: &AppHandle, payload: &OutEventPayload) {
    let name = event_name(payload);
    match app_handle.emit_all(name, payload) {
        Ok(_) => {
            let payload_json = serde_json::to_string(payload)
                .unwrap_or_else(|_| "<serialize failed>".into());
            log::info!("emit event: {} {}", name, payload_json);
        }
        Err(err) => log::error!("event emit failed: {}", err),
    }
}
