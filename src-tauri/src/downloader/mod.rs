/// Events relevant to downloading a file.
mod events;
pub use events::*;

/// Handles state management for file downloads.
mod state;
pub use state::*;

/// Errors that may occur when downloading or writing a file.
mod error;
pub use error::*;

use dashmap::DashMap;
use futures_util::StreamExt;
use reqwest::{Client, Url};
use std::path::{Path, PathBuf};
use tokio::{
    fs::{remove_file, OpenOptions},
    io::{AsyncWriteExt, BufWriter},
};

/// Downloads and writes a file. Emits status updates via a callback.
///
/// # Arguments
///
/// * `states` - States of the currently tracked downloads.
/// * `id` - An arbitrary identifier for the download.
/// * `source_url` - The URL to stream the download from.
/// * `target_file` - The path of the file to create and write the download to.
/// * `emit_fn` - A callback used to emit status updates.
///
/// # Panics
///
/// Panics if an error occurs and cleaning the partially downloaded file fails.
///
/// # Errors
///
/// This function will return an error if downloading or writing the file fails.
/// See [`enum@Error`] for possible errors.
pub async fn download_file<'a>(
    states: &'a DashMap<String, State>,
    id: &'a str,
    source_url: Url,
    target_file: PathBuf,
    emit_fn: impl Fn(Event),
) -> Result<(), DownloaderError> {
    let emit = |data: EventData| emit_fn(Event { id, data });

    match status_of(states, id) {
        Some(Status::Starting) | Some(Status::Active) => {
            return Err(DownloaderError::Duplicate)
        }
        None | Some(Status::Complete) | Some(Status::Failed) => {
            states.remove(id);
        }
    }

    clean_file(id, &target_file).await;

    let result = async {
        track_state(states, id, source_url.clone(), target_file.clone())
            .or(Err(DownloaderError::Duplicate))?;

        let response = Client::new()
            .get(source_url.as_str())
            .send()
            .await
            .map_err(DownloaderError::Request)?;
        let content_length = match response.content_length() {
            None => return Err(DownloaderError::ContentLengthNone),
            Some(0) => return Err(DownloaderError::ContentLengthZero),
            Some(len) => len,
        };
        let file = OpenOptions::new()
            .write(true)
            .create_new(true)
            .open(&target_file)
            .await
            .map_err(DownloaderError::CreateFile)?;
        let mut writer = BufWriter::new(file);
        let mut stream = response.bytes_stream();
        let mut downloaded: u64 = 0;

        set_active(states, id, content_length)
            .or(Err(DownloaderError::State))?;
        emit(EventData::Started { content_length });

        while let Some(chunk_result) = stream.next().await {
            let chunk = chunk_result.map_err(DownloaderError::ReadStream)?;
            downloaded += chunk.len() as u64;

            writer
                .write_all(&chunk)
                .await
                .map_err(DownloaderError::WriteFile)?;
            set_progress(states, id, chunk.len() as u64)
                .or(Err(DownloaderError::State))?;
            emit(EventData::Progress { downloaded });
        }

        writer.flush().await.map_err(DownloaderError::FlushFile)?;
        set_complete(states, id).or(Err(DownloaderError::State))?;
        emit(EventData::Complete);

        Ok(())
    }
    .await;

    if let Err(err) = result {
        set_failed(states, id).or(Err(DownloaderError::State))?;
        emit(EventData::Error {
            error_message: &err.to_string(),
        });
        clean_file(id, &target_file).await;

        return Err(err);
    }

    Ok(())
}

/// Deletes a partially downloaded file.
///
/// # Panics
///
/// Panics if cleaning the partially downloaded file fails.
///
/// # Arguments
///
/// * `target_file` - Path of the file to clean.
async fn clean_file(id: &str, target_file: &Path) {
    if !target_file.exists() {
        return;
    }

    if let Err(err) = remove_file(target_file).await {
        panic!(
            "failed to clean partially downloaded file: {} (id: {}) ({})",
            target_file.to_str().unwrap_or("<unknown>"),
            id,
            err
        );
    }
}
