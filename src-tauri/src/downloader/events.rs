use serde::Serialize;

/// Kinds of updates about the status of a file download.
#[derive(Serialize)]
#[serde(tag = "kind")]
pub enum EventData<'a> {
    /// Signals that a download has started.
    Started {
        /// The Content-Length of a download.
        content_length: u64,
    },

    /// Signals the progress of a download.
    Progress {
        /// The number of bytes that have been downloaded.
        downloaded: u64,
    },

    /// Signals that a download has completed successfully.
    Complete,

    /// Signals that the download has failed.
    Error {
        /// An error message to aid in debugging.
        error_message: &'a String,
    },
}

/// Wraps [`EventData`] to include the download id.
#[derive(Serialize)]
pub struct Event<'a> {
    /// Signals which download this event pertains to.
    pub id: &'a str,

    /// The event's payload.
    #[serde(flatten)]
    pub data: EventData<'a>,
}
