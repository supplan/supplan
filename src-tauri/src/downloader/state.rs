use std::path::PathBuf;

use dashmap::DashMap;
use reqwest::Url;

/// A concurrent hash map used to track the state of file downloads.
pub type TrackedDownloads = DashMap<String, State>;

/// Kinds of statuses that pertain to a file download.
#[derive(Clone, Copy)]
pub enum Status {
    /// Indicates that the download is starting. This status applies from before
    /// the GET request is sent to until reading the download stream begins.
    Starting,

    /// Indicates that the download stream is being read, and the content is
    /// being written to the target file.
    Active,

    /// Indicates that the download stream has been read to completion, and the
    /// file writer has been flushed.
    Complete,

    /// Indicates that an error occured any time during the download operation.
    Failed,
}

/// Represents the full state of a file download.
pub struct State {
    /// The status of the file download.
    pub status: Status,

    /// The URL to stream the download from.
    pub source_url: Url,

    /// The path of the file to create and write the download to.
    pub target_file: PathBuf,

    /// The size of the download, in bytes.
    pub content_length: Option<u64>,

    /// How many bytes have been downloaded (and written) so far.
    pub downloaded: u64,
}

/// Registers a new download to track and manage.
///
/// # Panics
///
/// Panics if an existing [`State`] is overwritten. The guard clause
/// *should* always prevent this.
pub fn track_state(
    states: &TrackedDownloads,
    id: &str,
    source_url: Url,
    target_file: PathBuf,
) -> Result<(), ()> {
    if states.contains_key(id) {
        return Err(());
    }

    let state = State {
        status: Status::Starting,
        source_url,
        target_file,
        content_length: None,
        downloaded: 0,
    };

    if states.insert(id.into(), state).is_some() {
        panic!("attempted to replace an existing download");
    }

    Ok(())
}

/// Gets the status of a tracked download.
///
/// # Arguments
///
/// * `states` - States of the currently tracked downloads.
/// * `id` - The download's identifier.
pub fn status_of(states: &TrackedDownloads, id: &str) -> Option<Status> {
    states.get(id).map(|s| s.status)
}

/// Marks a tracked download as active and sets its content length.
///
/// # Arguments
///
/// * `states` - States of the currently tracked downloads.
/// * `id` - The download's identifier.
/// * `content_length` - The size of the download, in bytes.
pub fn set_active(
    states: &TrackedDownloads,
    id: &str,
    content_length: u64,
) -> Result<(), ()> {
    let mut state = states.get_mut(id).ok_or(())?;
    state.content_length = content_length.into();
    state.status = Status::Active;

    Ok(())
}

/// Increases the progress of a tracked download.
///
/// # Arguments
///
/// * `states` - States of the currently tracked downloads.
/// * `id` - The download's identifier.
/// * `increase_by` - How many bytes to increase the download's progress by.
pub fn set_progress(
    states: &TrackedDownloads,
    id: &str,
    increase_by: u64,
) -> Result<(), ()> {
    let mut state = states.get_mut(id).ok_or(())?;
    state.downloaded += increase_by;

    Ok(())
}

/// Marks a tracked download as complete.
///
/// # Arguments
///
/// * `states` - States of the currently tracked downloads.
/// * `id` - The download's identifier.
pub fn set_complete(states: &TrackedDownloads, id: &str) -> Result<(), ()> {
    let mut state = states.get_mut(id).ok_or(())?;
    state.status = Status::Complete;

    Ok(())
}

/// Marks a tracked download as failed.
///
/// # Arguments
///
/// * `states` - States of the currently tracked downloads.
/// * `id` - The download's identifier.
pub fn set_failed(states: &TrackedDownloads, id: &str) -> Result<(), ()> {
    let mut state = states.get_mut(id).ok_or(())?;
    state.status = Status::Failed;

    Ok(())
}
