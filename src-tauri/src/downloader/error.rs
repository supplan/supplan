/// Errors that may occur when downloading or writing a file.
#[derive(thiserror::Error, Debug)]
pub enum DownloaderError {
    /// Occurs when a duplicate download is already in progress.
    #[error("duplicate download")]
    Duplicate,

    /// Occurs when a modifying the state of a tracked download fails.
    #[error("duplicate download")]
    State,

    /// Occurs when a GET request fails.
    #[error("request failed: {0}")]
    Request(#[source] reqwest::Error),

    /// Occurs when getting a response's Content-Length fails.
    #[error("failed to get content length")]
    ContentLengthNone,

    /// Occurs when a response's Content-Length is zero.
    #[error("content length is zero")]
    ContentLengthZero,

    /// Occurs when reading a download stream fails.
    #[error("download stream failed: {0}")]
    ReadStream(#[source] reqwest::Error),

    /// Occurs when creating a file fails.
    #[error("failed to create file: {0}")]
    CreateFile(#[source] tokio::io::Error),

    /// Occurs when writing to a file fails.
    #[error("write operation failed: {0}")]
    WriteFile(#[source] tokio::io::Error),

    /// Occurs when flushing the [`BufWriter`] fails.
    #[error("final flush failed: {0}")]
    FlushFile(#[source] tokio::io::Error),
}
