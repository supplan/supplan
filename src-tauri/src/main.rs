#![allow(unused)]
// #![warn(clippy::missing_docs_in_private_items)]
#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

//! Backend for supplan.

use std::{fs::File, sync::Arc};

use antidote::Mutex;
use dsld::archive_manager::ThreadSafeArchiveManager;
use simplelog::*;

/// Handles file downloads.
mod downloader;

/// Helps ensure type-safety for Tauri events.
mod events;

/// Manages the NIH ODS DSLD.
mod dsld;

/// Deserializer and serializer functions.
mod serde_fn;

/// Initialize app and register commands
fn main() {
    tauri::Builder::default()
        .setup(|_| {
            println!("[supplan]");

            CombinedLogger::init(vec![
                TermLogger::new(
                    LevelFilter::Info,
                    Config::default(),
                    TerminalMode::Stdout,
                    ColorChoice::Auto,
                ),
                WriteLogger::new(
                    LevelFilter::Info,
                    Config::default(),
                    File::create("my_rust_binary.log").unwrap(),
                ),
            ])
            .unwrap();

            Ok(())
        })
        .manage(downloader::TrackedDownloads::new())
        .manage::<ThreadSafeArchiveManager>(Arc::new(Mutex::new(
            dsld::archive_manager::ArchiveManager::new(),
        )))
        .invoke_handler(tauri::generate_handler![dsld::commands::download_dsld])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
