use chrono::NaiveDate;
use serde::{de, Deserialize, Deserializer, Serializer};

/// OPTIONAL: Deserializes a string in `YYYY-MM-DD` format to a [`NaiveDate`].
pub fn de_opt_str_to_date_ymd<'de, D>(
    deserializer: D,
) -> Result<Option<NaiveDate>, D::Error>
where
    D: Deserializer<'de>,
{
    let s: &str = Deserialize::deserialize(deserializer)?;
    match NaiveDate::parse_from_str(s, "%Y-%m-%d") {
        Ok(d) => Ok(Some(d)),
        Err(e) => Err(de::Error::custom(format!(
            "failed to deserialize string '{s}' date ({e})"
        ))),
    }
}

/// Deserializes a string in `YYYY-MM-DD` format to a [`NaiveDate`].
pub fn de_str_to_date_ymd<'de, D>(
    deserializer: D,
) -> Result<NaiveDate, D::Error>
where
    D: Deserializer<'de>,
{
    let s: &str = Deserialize::deserialize(deserializer)?;
    match NaiveDate::parse_from_str(s, "%Y-%m-%d") {
        Ok(d) => Ok(d),
        Err(e) => Err(de::Error::custom(format!(
            "failed to deserialize string '{s}' date ({e})"
        ))),
    }
}

/// OPTIONAL: Deserializes a string in `B DD, YYYY` format to a [`NaiveDate`].
pub fn de_opt_str_to_date_bdy<'de, D>(
    deserializer: D,
) -> Result<Option<NaiveDate>, D::Error>
where
    D: Deserializer<'de>,
{
    let s: &str = Deserialize::deserialize(deserializer)?;
    match NaiveDate::parse_from_str(s, "%B %d, %Y") {
        Ok(d) => Ok(Some(d)),
        Err(e) => Err(de::Error::custom(format!(
            "failed to deserialize string '{s}' date ({e})"
        ))),
    }
}

// /// Deserializes a string in `B DD, YYYY` format to a [`NaiveDate`].
// pub fn de_str_to_date_bdy<'de, D>(
//     deserializer: D,
// ) -> Result<NaiveDate, D::Error>
// where
//     D: Deserializer<'de>,
// {
//     let s: &str = Deserialize::deserialize(deserializer)?;
//     match NaiveDate::parse_from_str(s, "%B %d, %Y") {
//         Ok(d) => Ok(d),
//         Err(_) => Err(de::Error::custom("failed to deserialize date")),
//     }
// }

/// OPTIONAL: Serializes a [`NaiveDate`] to a string in ISO 8601 format.
pub fn ser_opt_date_to_str<S>(
    date: &Option<NaiveDate>,
    s: S,
) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    match date {
        Some(d) => s.serialize_str(d.format("%+").to_string().as_str()),
        None => s.serialize_none(),
    }
}

/// Serializes a [`NaiveDate`] to a string in ISO 8601 format.
pub fn ser_date_to_str<S>(date: &NaiveDate, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.serialize_str(date.format("%+").to_string().as_str())
}

/// Deserializes a string `"1" | "0" | "True" | "False"` to a boolean.
pub fn de_str_to_bool<'de, D>(deserializer: D) -> Result<bool, D::Error>
where
    D: Deserializer<'de>,
{
    let s: &str = Deserialize::deserialize(deserializer)?;
    match s.to_lowercase().as_str() {
        "1" | "true" => Ok(true),
        "0" | "false" => Ok(false),
        _ => Err(de::Error::custom(format!(
            "failed to deserialize string '{s}' to bool"
        ))),
    }
}

/// OPTIONAL: Deserializes a string `"1" | "0" | "True" | "False"` to a boolean.
pub fn de_opt_str_to_bool<'de, D>(
    deserializer: D,
) -> Result<Option<bool>, D::Error>
where
    D: Deserializer<'de>,
{
    let s: &str = Deserialize::deserialize(deserializer)?;
    match s.to_lowercase().as_str() {
        "1" | "true" => Ok(Some(true)),
        "0" | "false" => Ok(Some(false)),
        _ => Err(de::Error::custom(format!(
            "failed to deserialize string '{s}' to bool"
        ))),
    }
}

/// Deserializes a string to an i32.
pub fn de_str_to_i32<'de, D>(deserializer: D) -> Result<i32, D::Error>
where
    D: Deserializer<'de>,
{
    let mut s: &str = Deserialize::deserialize(deserializer)?;

    if s.contains('.') {
        s = s.split_once('.').unwrap_or_default().0;
    }

    s.parse::<i32>().map_err(|e| {
        de::Error::custom(format!(
            "failed to deserialize string '{s}' to i32 ({e})"
        ))
    })
}

/// OPTIONAL: Deserializes a string to an i32.
pub fn de_opt_str_to_i32<'de, D>(
    deserializer: D,
) -> Result<Option<i32>, D::Error>
where
    D: Deserializer<'de>,
{
    let opt_s: Option<&str> = Option::deserialize(deserializer)?;

    Ok(match opt_s {
        Some(mut s) => {
            if s.contains('.') {
                s = s.split_once('.').unwrap_or_default().0;
            }

            let opt_val = s.parse::<i32>().map_err(|e| {
                de::Error::custom(format!(
                    "failed to deserialize string '{s}' to i32 ({e})"
                ))
            });

            match opt_val {
                Ok(val) => Some(val),
                Err(e) => return Err(e),
            }
        }
        None => None,
    })
}

// /// OPTIONAL: Deserializes a string to an f32.
// pub fn de_opt_str_to_f32<'de, D>(
//     deserializer: D,
// ) -> Result<Option<f32>, D::Error>
// where
//     D: Deserializer<'de>,
// {
//     Ok(match Option::<&str>::deserialize(deserializer)? {
//         Some(s) => Some(s.parse::<f32>().map_err(|e| {
//             de::Error::custom(format!(
//                 "failed to deserialize string '{s}' to f32 ({e})"
//             ))
//         })?),
//         None => None,
//     })
// }

/// OPTIONAL: Deserializes a string to an f64.
pub fn de_opt_str_to_f64<'de, D>(
    deserializer: D,
) -> Result<Option<f64>, D::Error>
where
    D: Deserializer<'de>,
{
    Ok(match Option::<&str>::deserialize(deserializer)? {
        Some(s) => Some(s.parse::<f64>().map_err(|e| {
            de::Error::custom(format!(
                "failed to deserialize string '{s}' to f64 ({e})"
            ))
        })?),
        None => None,
    })
}
